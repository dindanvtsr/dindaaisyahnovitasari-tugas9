import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
// import {BASE_URL, TOKEN} from './url';
// import Axios from 'axios';

import ApiProvider from '../data/ApiProvider';

const EditData = ({navigation, route}) => {
  const [namaMobil, setNamaMobil] = useState('');
  const [totalKM, setTotalKM] = useState('');
  const [hargaMobil, setHargaMobil] = useState('');

  useEffect(() => {
    if (route.params) {
      const data = route.params;
      setNamaMobil(data.title);
      setTotalKM(data.totalKM);
      setHargaMobil(data.harga);
    }
  }, []);

  var dataMobil = route.params;

  const editData = async () => {
    if (!namaMobil || !totalKM || !hargaMobil) {
      alert('Please fill the empty field');
      return;
    }
    const body = [
      {
        _uuid: dataMobil._uuid,
        title: namaMobil,
        harga: hargaMobil,
        totalKM: totalKM,
        unitImage:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrhVioZcYZix5OUz8iGpzfkBJDzc7qPURKJQ&usqp=CAU',
      },
    ];

    // const options = {
    //   headers: {
    //     'Content-Type': 'application/json',
    //     Authorization: TOKEN,
    //   },
    // };

    const response = await ApiProvider.addDataMobil(body);
    if (response.status === 200 || response.status === 201) {
      alert('Data mobil berhasil diubah');
      navigation.navigate('Home');
    } else {
      alert('Data mobil gagal diubah');
    }
    // Axios.put(`${BASE_URL}mobil`, body, options)
    //   .then(response => {
    //     console.log('Response edit success', response);
    //     if (response.status === 200 || response.status === 201) {
    //       alert('Data mobil berhasil diubah');
    //       navigation.navigate('Home');
    //     }
    //   })
    //   .catch(error => {
    //     console.log('error edit', error);
    //   });
  };

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <View
        style={{
          width: '100%',
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={{
            width: '10%',
            justifyContent: 'center',
            alignItems: 'center',
            paddingVertical: 10,
          }}>
          <Icon name="arrowleft" size={20} color="#000" />
        </TouchableOpacity>
        <Text style={{fontSize: 16, fontWeight: 'bold', color: '#000'}}>
          Edit Data
        </Text>
      </View>
      <View
        style={{
          width: '100%',
          padding: 15,
        }}>
        <View>
          <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
            Nama Mobil
          </Text>
          <TextInput
            placeholder="Masukkan Nama Mobil"
            style={styles.txtInput}
            onChangeText={text => setNamaMobil(text)}
            value={namaMobil}
          />
        </View>
        <View style={{marginTop: 20}}>
          <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
            Total Kilometer
          </Text>
          <TextInput
            placeholder="contoh: 100 KM"
            style={styles.txtInput}
            onChangeText={text => setTotalKM(text)}
            value={totalKM}
          />
        </View>
        <View style={{marginTop: 20}}>
          <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
            Harga Mobil
          </Text>
          <TextInput
            placeholder="Masukkan Harga Mobil"
            style={styles.txtInput}
            keyboardType="number-pad"
            onChangeText={text => setHargaMobil(text)}
            value={hargaMobil}
          />
        </View>
        <TouchableOpacity style={styles.btnAdd} onPress={() => editData()}>
          <Text style={{color: '#fff', fontWeight: '600'}}>Ubah Data</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  btnAdd: {
    marginTop: 20,
    width: '100%',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: '#689f38',
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtInput: {
    marginTop: 10,
    width: '100%',
    borderRadius: 6,
    paddingHorizontal: 10,
    borderColor: '#dedede',
    borderWidth: 1,
  },
});

export default EditData;
