import {TOKEN} from '../simpleNetworking/url';
import API from './ApiConfig';

export default {
  getDataMobil: async () => {
    return API('mobil', {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: TOKEN,
      },
    })
      .then(response => {
        console.log('res get mobil', response);
        return response;
      })
      .catch(err => {
        console.log('error get mobil', err);
        return err;
      });
  },

  addDataMobil: async mobil => {
    return API('mobil', {
      method: 'POST',
      head: {
        'Content-Type': 'application/json',
        Authorization: TOKEN,
      },
      body: mobil,
    })
      .then(response => {
        console.log('Response add success', response);
        return response;
      })
      .catch(err => {
        console.log('error add', err);
        return err;
      });
  },

  editDataMobil: async mobil => {
    return API('mobil', {
      method: 'PUT',
      head: {
        'Content-Type': 'application/json',
        Authorization: TOKEN,
      },
      body: mobil,
    })
      .then(response => {
        console.log('Response edit success', response);
        return response;
      })
      .catch(err => {
        console.log('error add', err);
        return err;
      });
  },

  deleteDataMobil: async mobil => {
    return API('mobil', {
      method: 'DELETE',
      head: {
        'Content-Type': 'application/json',
        Authorization: TOKEN,
      },
      body: mobil,
    })
      .then(response => {
        console.log('res delete mobil', response);
        return response;
      })
      .catch(err => {
        console.log('error delete mobil', err);
        return err;
      });
  },
};
